﻿using UnityEngine;
using Rewired;

[RequireComponent(typeof(CharacterController))]
public class PlayerCharacter : MonoBehaviour {

    public float playerSpeed = 2.0f;
    public float rotationSpeed = 2.0f;

    // Rewired Player
    private Player player;
    
    // Input Variables
    private Vector3 movement = new Vector3();
    private CharacterController cc;
    private bool fire;
    private Vector3 rotation = new Vector3();

    // Make sure that the player is above the ground
    private const float PLAYER_Y = 0.5f;

    // Make sure that the camera can see the player
    private const float CAMERA_Y = 6f;

    private MazeCell currentCell;

    // Time elapsed for controlling bullet fire rate
    protected float elapsedTime;

    // Bullet variables
    public GameObject bullet;
    public GameObject bulletSpawnPoint;
    public float fireRate = 2.0f;

    // Health

    // Use this for initialization
    void Start () {
        player = ReInput.players.GetPlayer(0);

        cc = GetComponent<CharacterController>();

        Camera.main.transform.position = new Vector3(transform.position.x, CAMERA_Y, transform.position.z);

        // Immediately reset to zero
        elapsedTime = 0.0f;
    }

    public void SetLocation(MazeCell cell)
    {
        currentCell = cell;
        Vector3 localPosition = cell.transform.localPosition;
        transform.localPosition = new Vector3(localPosition.x, PLAYER_Y, localPosition.z);
    }
	
	// Update is called once per frame
	void Update () {
        GetInput();
        ProcessInput();

        // Update this so we can control the fire rate of bullets
        elapsedTime += Time.deltaTime;
    }

    private void GetInput()
    {
        movement.x = player.GetAxis("Move Horizontal");
        movement.z = player.GetAxis("Move Vertical");
        rotation.x = player.GetAxis("Rotate Horizontal");
        rotation.z = player.GetAxis("Rotate Vertical");
        fire = player.GetButton("fire");
    }

    private void ProcessInput()
    {
        // Process Movement event
        if (movement.x != 0.0f || movement.y != 0.0f)
        {
            cc.Move(movement * playerSpeed * Time.deltaTime);
            Camera.main.transform.position = new Vector3(transform.position.x, CAMERA_Y, transform.position.z);
        }

        // Process Rotation event - figure out a way to slowly rotate back to 0
        if (rotation.sqrMagnitude > 0.1f)
            cc.transform.LookAt(transform.position + rotation);

        float angle = Mathf.Atan2(rotation.x, rotation.z) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.Euler(new Vector3(0, angle, 0));

        Quaternion.Slerp(transform.rotation, rot, rotationSpeed * Time.deltaTime);

        // Process Fire event
        if (fire)
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (elapsedTime >= fireRate)
        {
            Instantiate(bullet, bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.rotation);
            elapsedTime = 0.0f;
        }
    }
}
