﻿using UnityEngine;

public class MazeDoor : MazePassage {

    private static Quaternion normalRotation = Quaternion.Euler(0f, -90f, 0f);
    private static Quaternion mirroredRotation = Quaternion.Euler(0f, 90f, 0f);

    private bool isMirrored;

    public float moveSpeed = 4.0f;

    // Door opening and closing
    private bool doorClosed = true;
    private bool doorOpened = false;
    private bool doorOpening = false;
    private bool doorClosing = false;

    private bool playerInTrigger = false;

    private MazeDoor OtherSideOfDoor
    {
        get
        {
            return otherCell.GetEdge(direction.GetOpposite()) as MazeDoor;
        }
    }

    public override void Initialize(MazeCell primary, MazeCell other, MazeDirection direction)
    {
        base.Initialize(primary, other, direction);
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            child.GetComponent<Renderer>().material = cell.room.settings.wallMaterial;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerInTrigger = true;
        }
    }

    void Update()
    {
        if (doorClosed)
        {

        }
    }
}
